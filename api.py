import json
from flask import Flask, jsonify, request
import requests

app = Flask(__name__)

@app.route('/character',methods=["GET"])
def getCharacter():
	try:
		req = requests.get("https://rickandmortyapi.com/api/character")
		print(req)
		if req.status_code == 200:
			return jsonify({"data":req.json()["results"]})
	except Exception as error:
		print(error)
		return jsonify(error="Parametros incorrectos"), 500


def main():

	#PUERTO_FLASK = os.environ['PUERTO_FLASK']
	app.run(host='0.0.0.0', debug=True, port=8080)


if __name__=='__main__':
	main()